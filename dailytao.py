import urllib.request
import datetime

url = 'http://dailytao.org'
response = urllib.request.urlopen(url)
parsing_tao = False

for line_bytes in response:
    line = line_bytes.decode('utf-8')

    if parsing_tao is True:
        temp_line = line.split("<", 1)[0]
        print(temp_line)

        if "</p>" in line:
            break

    if "<p>" in line:
        parsing_tao = True

print("\n\t- Lao Tzu")
